{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "baa9b854-0b9f-4a24-a6c4-85113726230b",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Uncertainty Quantification\n",
    "\n",
    "#### Prof. Dr. Martin Frank\n",
    "\n",
    "---\n",
    "\n",
    "## **Sensitivities**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3322724b-1490-4965-a2c5-49825e11e90b",
   "metadata": {},
   "source": [
    "### **11 Sensitivities**\n",
    "\n",
    "Recall our abstract formulation: We have a QoI $R(u(Z))$ where input and solution are implicitly related by $\\mathcal{E}(u,Z)=0$.\n",
    "\n",
    "**Example.**\n",
    "$$\n",
    "\\begin{array}{l}\n",
    "\\frac{d}{d t} u(t,Z)-Z_1 u(t,Z)=0\\\\\n",
    "u(0,Z)-Z_2=0\n",
    "\\end{array}\n",
    "\\quad \\Bigg\\} \\quad \\mathcal{E}(u,Z)=0\n",
    "$$\n",
    "with\n",
    "$$\n",
    "R(u(Z))=\\mathrm{E}[u(T,Z)].\n",
    "$$\n",
    "\n",
    "We give a different name to the QoI, when we consider it as function of $Z$ directly. \n",
    "Define the reduced QoI \n",
    "$$\\tilde{R}(Z)=R(u(Z)).$$ \n",
    "Sensitivities are the partial derivatives of the reduced QoI with respect to the input. Now interpreting $Z$ as a parameter and applying a Taylor expansion around $Z^0$\n",
    "\\begin{align*}\n",
    "\\underbrace{\\tilde{R}(Z)}_{\\in\\mathbb{R}}=\\underbrace{\\tilde{R}(Z^0)}_{\\in\\mathbb{R}}+\\overbrace{\\underbrace{\\frac{\\partial \\tilde{R}(Z^0)}{\\partial Z}}_{\\in\\mathbb{R}^{1\\times d}}}^{\\text{sensitivities}}\\underbrace{(Z-Z^0)}_{\\in\\mathbb{R}^d}+\\mathcal{O}\\left(\\left( Z-Z^0\\right)^2\\right)\n",
    "\\end{align*}\n",
    "What about expected value and variance?\n",
    "\n",
    "Assume that $Z$ has the pdf $f$. Then\n",
    "\\begin{align*}\n",
    "\\mathrm{E}[\\tilde{R}(Z)]&=\\int\\tilde{R}(z) f(z)dz\\\\\n",
    "&\\approx\\int\\left(\\tilde{R}(Z^0)+\\frac{\\partial \\tilde{R}(Z^0)}{\\partial z}(z-Z^0)\\right) f(z)dz\\\\\n",
    "&=\\tilde{R}(Z^0)+\\frac{\\partial \\tilde{R}(Z^0)}{\\partial z}\\int\\left( z-Z^0\\right) f(z)dz\n",
    "\\end{align*}\n",
    "If we choose to linearize around $Z^0=\\mathrm{E}[Z]$, then $\\mathrm{E}[\\tilde{R}(Z)]=\\tilde{R}(\\mathrm{E}[Z])$. Furthermore\n",
    "\\begin{align*}\n",
    "\\mathrm{Var}\\left[\\tilde{R}(Z)\\right]&= \\mathrm{E}\\left[\\left(\\tilde{R}(Z)-\\tilde{R}\\left( Z^0\\right)\\right)^2\\right]=\\mathrm{E}\\left[\\left(\\tilde{R}(Z)\\right)^2\\right]\\\\\n",
    "&\\approx\\int\\bigg{(}\\underbrace{\\frac{\\partial \\tilde{R}(Z^0)}{\\partial z}}_{:= S^T}(z-Z^0)\\bigg{)}^2 f(z)dz\\\\\n",
    "&=\\int S^{T}(z-Z^0)(z-Z^0)^{T}Sf(z)dz\\\\\n",
    "&=S^{T}\\underbrace{\\int (z-Z^0)(z-Z^0)^{T}f(z)}_{\\mathrm{Cov}(Z)\\in\\mathbb{R}^{d\\times d}}dz\\ S\\\\\n",
    "&=S^{T}\\mathrm{Cov}(Z)\\ S\n",
    "\\end{align*}\n",
    "We know this as the *sandwich rule*.\n",
    "\n",
    "**Remark.** \n",
    "- In a linearized/linear model, sensitivities are directly related to statistical quantities.\n",
    "- Sensitivities, moreover, give us quantitative information on the effect of a certain input variable on the output.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e43f3d3-1d63-482f-99d4-84208d8debbf",
   "metadata": {
    "tags": []
   },
   "source": [
    "---\n",
    "### **Exercises**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7355353a-32a7-4522-989c-8521c72f717e",
   "metadata": {},
   "source": [
    "**EXERCISE 11.1)**\n",
    "\n",
    "Consider the ODE with uncertainties\n",
    "$$\n",
    "\\dot u(t,z) = -z_1 u(t,z),\\quad u(0,z)=z_2.\n",
    "$$ \n",
    "Derive expressions for the\n",
    "sensitivities of \n",
    "$$R(u(z)) = \\int_0^T u(t,z)dt$$ \n",
    "with respect to $z_1$ and $z_2$ by\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   (a) using the explicit solution,\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   (b) using forward differentiation,\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   (c) using adjoints."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0434b4c4-0e73-4934-b3fd-95ce1af580be",
   "metadata": {},
   "source": [
    "**Solutions:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f6c8ae20-0dcd-499e-b940-666418838adb",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "#(Run this cell to load solutions)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/11_1_a.PNG',width=600))\n",
    "display(Image(filename='solutions/11_1_b.png',width=600))\n",
    "display(Image(filename='solutions/11_1_c.PNG',width=600))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4034846a-0327-4ff6-926f-70206e67bb3a",
   "metadata": {},
   "source": [
    "**EXERCISE 11.2)**\n",
    "\n",
    "Assume a (discretized) system can be written in the form\n",
    "$$ E(u,z) = 0,$$\n",
    "\n",
    "where\n",
    "- the solution $u$ $\\in \\mathbb{R}^K$\n",
    "- the parameters $z$ $\\in \\mathbb{R}^N$\n",
    "\n",
    "and the constraint E: $\\mathbb{R}^K \\times \\mathbb{R}^N \\rightarrow \\mathbb{R}^K$ admits a unique solution for $u$ depending on $z$. Assume further that we have quantities of interest\n",
    "$$R(u) = R(u(z))$$\n",
    "with $R: \\mathbb{R}^K \\rightarrow \\mathbb{R}^I$. We are interested in the sensitivity of $R$ with respect to $z$, i.e.\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   (a) Derive formulas for the sensitivities by forward-differentiation.\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   (b) Derive formulas for the sensitivities by adjoint calculus.\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   (c) Discuss the dimensions of all objects. When is forward differentiation advantageous, when adjoints?\n",
    "\n",
    "&nbsp;\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
