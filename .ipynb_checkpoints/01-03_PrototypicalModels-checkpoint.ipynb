{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "325fa5eb-d704-40ba-9722-4d0ff86fc62c",
   "metadata": {},
   "source": [
    "### Uncertainty Quantification\n",
    "\n",
    "#### Prof. Dr. Martin Frank\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d9c2da90-0031-444c-af77-87e8b040bcb1",
   "metadata": {
    "tags": []
   },
   "source": [
    "### **1  Prototypical Models**\n",
    "\n",
    "**Example.** *(Algebraic equation)* \n",
    "\n",
    "Maximal amplitude of harmonic oscillator\n",
    "\\begin{equation}\n",
    "Z_0 = \\frac{f_0}{\\sqrt{m^2(\\omega_0^2-\\omega_F^2)^2}+c^2\\omega_F^2},\\quad \\omega_0=\\frac{K}{m}\n",
    "\\end{equation}\n",
    "with mass $m$, damping constant $K$, stiffness $c$.\n",
    "\n",
    "<img src=\"img/Response.png\" alt=\"image\" width=\"400\" height=\"auto\" style=\"display: block; margin: 0 auto\" >\n",
    "\n",
    "**Example.** *(ODE)* \n",
    "\n",
    "Initial value problem under uncertainty\n",
    "\\begin{align*}\n",
    "\\frac{d}{dt}u(t;\\alpha,\\beta) &= -\\alpha u(t;\\alpha,\\beta) \\\\\n",
    "u(0;\\alpha,\\beta) &= \\beta\n",
    "\\end{align*}\n",
    "Uncertain initial condition $\\beta$. Uncertain decay parameter $\\alpha$.\n",
    "\n",
    "**Example.** *(PDE)* \n",
    "\n",
    "Heat equation under uncertainty\n",
    "\\begin{align*}\n",
    "u_t &= \\nabla_x(\\alpha\\nabla_x u) \\\\\n",
    "u(0,x) &= u_0(x)\n",
    "\\end{align*}\n",
    "Uncertain diffusion constant $\\alpha$. Uncertain initial condition $u_0$.\n",
    "\n",
    "**Example.** *(The Curse of Dimensionality)* \n",
    "\n",
    "Initial value problem under uncertainty\n",
    "\\begin{align*}\n",
    "\\frac{d}{dt}u(t;\\alpha,\\beta) &= -\\alpha u(t;\\alpha,\\beta) \\\\\n",
    "u(0;\\alpha,\\beta) &= \\beta\n",
    "\\end{align*}\n",
    "Try different values for $\\alpha$ and $\\beta$, say 10 each. Total number of parameter combinations $10^2$ grows exponentially. \n",
    "Every unknown parameter adds a *dimension* to the problem.\n",
    "\n",
    "\n",
    "### **2  Notation**\n",
    "Let's fix some notation:\n",
    "\n",
    "- We describe the random input into a model by a vector-valued random variable $Z:\\Omega\\longmapsto\\mathbb{R}^d$ on $[0,1]^d$. Later we just write $Z\\in\\mathbb{R}^d$. Typically, the dimension $d$ will be large.\n",
    "- We denote each one-dimensional entry of the vector $Z$ as $Z_i$, $i=1,...,d$. A series of $N$ random input vectors will be denoted by $Z^{(i)}$\n",
    "- realisations of random vectors are typically denoted by small letters. Thus $z \\in \\mathbb{R}^d$ is the realisation of $Z$, $z_i \\in \\mathbb{R}$ of $Z_i$, $i=1,...,d$ and $Z^{(i)}\\in \\mathbb{R}^d$ of $z^{(i)}$ , $i=1,...,N$\n",
    "- We assume that $Z$ has a *probability density function* (pdf) \n",
    "\\begin{align*}\n",
    "f:\\mathbb{R}^{d}\\longmapsto\\mathbb{R}_{\\geq0}\n",
    "\\end{align*}\n",
    "so that the expected value $\\mathrm{E}[Z]$ and the variance $\\mathrm{Var}[Z]$ can be written as\n",
    "\\begin{align*}\n",
    "\\mathrm{E}[Z]&=\\int\\limits_{[0,1]^{d}\\,\\text{or}\\,\\mathbb{R}^d}zf(z) dz\\\\\n",
    "\\mathrm{Var}[Z]&=\\int\\limits_{[0,1]^{d}\\,\\text{or}\\,\\mathbb{R}^d}(z-\\mathrm{E}[Z])^2f(z) dz\n",
    "\\end{align*}\n",
    "\n",
    "- We also assume that $Z$ has a *cumulative distribution function* (cdf) $F$, which is the primitive of $f$, so that $F'=f$.\n",
    "\n",
    "- We denote the solution $u$ which might be a vector, a time-dependent function, or a space-time dependent function that also depends on the random input $Z$, by $u(Z)$, $u(t,Z)$ or $u(t,x,Z)$.\n",
    "\n",
    "- In all our problems, we have a \\textit{Quantity of Interest} (QoI), which we denote by $R(u(Z))$ or just $R(Z)$. Typically ,\n",
    "\\begin{align*}\n",
    "R(Z)= \\binom{\\mathrm{E}[u(Z)]}{\\mathrm{Var}[u(Z)]},\n",
    "\\end{align*}\n",
    "but we might also be interested in the whole probability density function.\n",
    "\n",
    "- Often, $u$ is given implicitly (e.g. as the solution of a differential equation). We denote the problem that $u$ solves by $\\mathcal{E}(u,Z)=0$.\n",
    "- We assume unique solvability, so we have a data-to-solution map which we write as $u=u(Z)$.\n",
    "\n",
    "**Example.** *(Ordinary differential equation (ODE))* \n",
    "$$\n",
    "\\begin{cases}\n",
    "\\frac{d}{dt} u(t,Z)= -Z_1 u(t,Z)\\\\\n",
    "u(0,Z)=Z_2,\n",
    "\\end{cases}$$\n",
    "Here, $Z\\in\\mathbb{R}^2$. Rewriting the problem as\n",
    "$$\n",
    "\\begin{array}{l}\n",
    "\\frac{d}{dt} u(t,Z)+Z_1 u(t,Z)=0\\\\\n",
    "u(0,Z)-Z_2=0\n",
    " \\end{array}  \\Bigg\\} \\, \\mathcal{E}(u,Z)=0\n",
    "$$\n",
    "Here, we can write down the solution\n",
    "\\begin{equation}\n",
    "u(t,Z)= Z_2e^{-Z_{1}t},\n",
    "\\end{equation}\n",
    "where $\\mathcal{S}$ is a map to the set of time-dependent functions $u$. The QoI might be\n",
    "\\begin{align*}\n",
    "R(u)=\\mathrm{E}[u(T,Z)]=\\mathrm{E}_{Z}[u(T,Z)]=\\mathrm{E}_{f}[u(T,Z)]=\\int z_2e^{-z_{1}T}f(z)\\ dz\n",
    "\\end{align*}\n",
    "\n",
    "**Example.** *(Ordinary differential equation (ODE))* \n",
    "$$\n",
    "\\begin{cases}\n",
    "\\frac{d}{dt} u(t,Z)= -\\alpha u(t,Z)\\\\\n",
    "u(0,Z)=Z,\n",
    "\\end{cases}\n",
    "$$\n",
    "Here, we only have one uncertain parameter. As before, the solution is $u(t,Z)= Ze^{-\\alpha t}$. Because the solution depends linearly on the input random variable $Z$, we can explicitly compute QoIs:\n",
    "\\begin{align*}\n",
    "\\mathrm{E}[u(t,Z)]= e^{-\\alpha t}\\mathrm{E}[Z] \n",
    "\\end{align*}\n",
    "and\n",
    "\\begin{align*}\n",
    "\\mathrm{Var}[u(t,Z)] = e^{-2\\alpha t}\\mathrm{Var}[Z] \n",
    "\\end{align*}\n",
    "\n",
    "### **3 The Sandwich Rule**\n",
    "\n",
    "Let's look at a general linear model with \n",
    "input $Z\\in\\mathbb{R}^d$, and output $u(Z)  \\in\\mathbb{R}$ which depends linearly on $Z$:\n",
    "$$\n",
    "u(Z) = \\mathcal{S}(Z) = \\sum_{i=1}^d a_iZ_i = a^TZ.\n",
    "$$\n",
    "Then\n",
    "\\begin{align*}\n",
    "\\mathrm{E}[u(Z)] &=\\sum_{i=1}^{a}a_{i}\\mathrm{E}[Z_i]= \\sum_{i=1}^{a}a_{i} \\int_{[0,1]^{d}\\,\\text{or}\\,\\mathbb{R}^d}z_{i}f(z)\\ dz\\\\\n",
    "&=a^{T}\\mathrm{E}[Z]=\\mathrm{E}[a^{\\mathrm{T}}Z]\n",
    "\\end{align*}\n",
    "and,\n",
    "\\begin{align*}\n",
    "\\mathrm{Var}[u]&=\\mathrm{E}\\left[ \\left( u-\\mathrm{E}[u] \\right)^2 \\right]\n",
    "=\\mathrm{E}\\left[ \\left( a^{\\mathrm{T}}Z-a^{\\mathrm{T}} \\mathrm{E}[Z] \\right)^2 \\right]\\\\\n",
    "&=\\mathrm{E}\\left[ \\left(a^{\\mathrm{T}}Z-a^{\\mathrm{T}}\\mathrm{E}[Z] \\right) \\left( Z^{\\mathrm{T}}a-\\mathrm{E}[Z]^{\\mathrm{T}}a\\right)\\right]\\\\\n",
    "&=a^{\\mathrm{T}}\\mathrm{E}\\left[\\left(Z-\\mathrm{E}[Z]\\right) \\left(Z^{\\mathrm{T}}-\\mathrm{E}[Z]^{\\mathrm{T}}\\right)\\right]a\\\\\n",
    "&=a^{\\mathrm{T}}\\mathrm{Cov}(Z)a\n",
    "\\end{align*}\n",
    "with the covariance matrix $\\mathrm{Cov}(Z)\\in\\mathbb{R}^{d\\times d}$ and $(\\mathrm{Cov}(Z))_{ij}=\\mathrm{Cov}(Z_i,Z_j)$. This formula is called the *sandwich rule*.\n",
    "\n",
    "For a linear (or linearized) data-to-solution map, it is sufficient to know the covariance of the input random variable. If input random variables are uncorrelated (the covariance matrix is diagonal), then they decouple in the sense that there are no mixed products anymore. Thus it might make sense to de-correlate input random variables before a stochastic simulation.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9b9a3e84-448a-426e-abf7-f5f48267231b",
   "metadata": {},
   "source": [
    "---\n",
    "### **Exercises**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16162fc1-457c-43a6-8994-e25574dba854",
   "metadata": {},
   "source": [
    "**EXERCISE 3.1)**\n",
    "\n",
    "A random vector $X=(X_1,\\dots,X_n)^T$ is said to be normally distributed\n",
    "if the probability density function is given by \n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "f_{X}(x) = \\frac{1}{(2\\pi)^{n/2}\\vert \\det{V}\\vert^{1/2}}\\text{exp}\\left( -\\frac{1}{2}(x-\\mu)^T V^{-1}(x-\\mu) \\right)\n",
    "\\end{aligned}\n",
    "$$\n",
    "where $\\mu = (\\mu_1,\\cdots,\\mu_n)$ is the mean and \n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "V = \n",
    "\\begin{pmatrix}\n",
    "\\text{var}(X_1) & \\text{cov}(X_1,X_2) & \\cdots & \\text{cov}(X_1,X_n) \\\\\n",
    "\\text{cov}(X_2,X_1) & \\text{var}(X_2) & & \\vdots \\\\\n",
    "\\vdots &  & \\ddots & \\\\\n",
    "& & & & \\\\\n",
    "\\text{cov}(X_n,X_1) & \\cdots &  & \\text{var}(X_n)\n",
    "\\end{pmatrix}\\end{aligned}\n",
    "$$ \n",
    "\n",
    "is the covariance matrix. The notation is\n",
    "$X \\sim \\mathcal{N}(\\mu,V)$. Let $Y=(Y_1,\\dots,Y_n)^T$ be a normally\n",
    "distributed random vector with distribution $\\mathcal{N}(\\mu,V)$, where\n",
    "$V$ is positive definite and has the Cholesky decomposition $V=LL^T$.\n",
    "Let $Z\\sim \\mathcal{N}(0,I_n)$, where $I_n$ is the $n\\times n$ identity\n",
    "matrix. Show that $Y$ can be written as \n",
    "\n",
    "$$\n",
    "Y= LZ+\\mu.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cdff5f9f-d9b6-497e-8ce3-6864518ce1f5",
   "metadata": {},
   "source": [
    "**Solution:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c308585f-3f67-4280-9b7d-2a382f023a94",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "#(Run this cell to load solution)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/03_1.png',width=600))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44a6ebbe-5a11-485f-a46b-15304fd102e0",
   "metadata": {},
   "source": [
    "**EXERCISE 3.2)**\n",
    "\n",
    "Let $X\\sim \\mathcal{N}(\\mu,\\sigma^2)$ be a normally distributed random\n",
    "variable. Look up inverse transform sampling, rejection sampling and the\n",
    "Box-Muller method in the literature and\n",
    "\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a)  Derive a formula for inverse transform sampling of $X$ using the\n",
    "    error function\n",
    " \n",
    "  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  $$\\text{erf}(x) = \\frac{2}{\\sqrt{\\pi}}\\int_0^x e^{-t^2}dt.$$\n",
    "\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b)   Derive a rejection algorithm to sample $X$ by using $e^{-|x|}$ to\n",
    "    generate an upper bound.\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(c)   Write down an algorithm for the Box-Muller method.\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(d) Add your previous derivations to the code below to obtain inverse transform sampling, rejection sampling, and the  Box-Muller method. \n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Run the code and compare their efficiency (also with `randn`).\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(e)   How would you sample from a multi-variate Gaussian random vector\n",
    "\n",
    "   $$ X\\sim \\mathcal{N}(\\mu,V)?$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5e8edc42-878d-4eb2-aaf5-7025e5c09051",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import math\n",
    "import scipy\n",
    "import scipy.special\n",
    "# exact pdf\n",
    "def f(x,mu,sigma):\n",
    "    return (1/np.sqrt(2*np.pi*sigma**2))*np.exp(-0.5*((x-mu)**2)/(sigma**2));\n",
    "\n",
    "# inverse cdf for density function exp(-|x|)\n",
    "def GInverse(U1):\n",
    "    \n",
    "    # TODO: define inverse cdf for density function exp(-|x|)\n",
    "    # ...\n",
    "    \n",
    "    return y\n",
    "\n",
    "# sample function of acceptReject\n",
    "def sampleNormal():\n",
    "    \n",
    "    # TODO: define sample function of acceptReject\n",
    "    # ...\n",
    "    \n",
    "    # if sample is accepted return reject = 1, otherwise reject = 2. Store result of sample on eta\n",
    "    return [reject,eta];"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "398edf26-8856-4f76-b953-efba151a5ed4",
   "metadata": {},
   "source": [
    "**Solution:**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ebc1a8bb-13c0-4c1b-99d1-d5f3a264f530",
   "metadata": {},
   "source": [
    "**a)** "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f1de719b-a852-4e3c-9f04-0ac321e27759",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "#(Run this cell to load solution)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/03_2_a.PNG',width=600))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "341d70d7-22c2-4d48-affc-1b8a8c347325",
   "metadata": {},
   "source": [
    "**b)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "244e0d80-96fb-4e1a-a4c7-356343f1eb81",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "#(Run this cell to load solution)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/03_2_b.PNG',width=600))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c99e8b41-eafb-493a-a518-1a2a986b1cbe",
   "metadata": {},
   "source": [
    "**c)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db17280b-3890-4162-8eee-95f80b5df81f",
   "metadata": {},
   "outputs": [],
   "source": [
    "#(Run this cell to load solution)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/03_2_c.PNG',width=600))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "323bc020-0dd8-4f12-9080-c9bf7cbc865e",
   "metadata": {},
   "source": [
    "**d)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "72214b5c-5012-4f6c-8112-1d0786447143",
   "metadata": {},
   "outputs": [],
   "source": [
    " #Run this cell to see a possible solution for d)\n",
    "#Run the code that appears again to see plot\n",
    "%load ./solutions/Sol032d.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36a5fa0d-5c31-4fa2-856d-24da6a4c64a8",
   "metadata": {},
   "source": [
    "**e)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8983c399-1955-494c-a124-0c472bdbbc98",
   "metadata": {},
   "outputs": [],
   "source": [
    "#(Run this cell to load solution)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/03_2_e.PNG',width=600))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b14cdf9c-5e62-410f-8e20-8292d79f77e5",
   "metadata": {
    "tags": []
   },
   "source": [
    "**EXERCISE 3.3)**\n",
    "\n",
    "Let $X$ be a uniformly distributed random variable on the interval\n",
    "$[a,b]$, i.e. $X$ has the pdf \n",
    "\n",
    "$$\n",
    "f_X(x) = \\frac{1}{b-a}\\chi_{[a,b]}(x).\n",
    "$$\n",
    "\n",
    "Show that\n",
    "\n",
    "$$\n",
    "\\text{E}[X] = \\frac{a+b}{2}\\text{ and } \\text{Var}[X] = \\frac{(b-a)^2}{12}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "762450d2-dfe9-42e8-a54b-e1729f63418a",
   "metadata": {},
   "source": [
    "**EXERCISE 3.4)**\n",
    "\n",
    "Let $X$ be a $\\text{Gamma}(\\alpha,\\beta)$-distributed random variable,\n",
    "i.e. $X$ has the pdf\n",
    "\n",
    "$$\n",
    "f_X(x) = \\frac{\\beta^\\alpha}{\\Gamma(\\alpha)}x^{\\alpha-1}e^{-\\beta x} \\text{ for } x>0.\n",
    "$$\n",
    "\n",
    "Show that\n",
    "\n",
    "$$\n",
    "E[X] = \\frac{\\alpha}{\\beta}\\text{ and } \\text{Var}[X] = \\frac{\\alpha}{\\beta^2}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "996a47fc-5ecd-4039-be59-f89c1a4e7e2a",
   "metadata": {
    "tags": []
   },
   "source": [
    "**EXERCISE 3.5)**\n",
    "\n",
    "Use the Cauchy-Schwarz inequality to show that the correlation\n",
    "\n",
    "$$\n",
    "\\text{corr}(X_1,X_2) = \\frac{\\text{cov}(X_1,X_2)}{\\sigma_{X_1}\\sigma_{X_2}}\n",
    "$$\n",
    "\n",
    "of two continuous random variables satisfies\n",
    "\n",
    "$$\n",
    "-1\\leq \\text{corr}(X_1,X_2)\\leq 1.\n",
    "$$"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
