import numpy as np
import matplotlib.pyplot as plt
import math
import scipy
import scipy.special
# exact pdf
def f(x,mu,sigma):
    return (1/np.sqrt(2*np.pi*sigma**2))*np.exp(-0.5*((x-mu)**2)/(sigma**2));

# inverse cdf for density function exp(-|x|)def GInverse(U1):
def GInverse(U1):
    if U1 < 0.5:
        y = np.log(2*U1);
    else:
        y = -np.log(-2*(U1-1));
    return y

# exact pdf
def f(x,mu,sigma):
    return (1/np.sqrt(2*np.pi*sigma**2))*np.exp(-0.5*((x-mu)**2)/(sigma**2));

# sample function of acceptReject
def sampleNormal():
    U1 = np.random.rand();
    U2 = np.random.rand();
    eta = GInverse(U1);
    xi = np.exp(-abs(eta))*U2;
    if xi < f(eta,0,1):
        reject  = 1;
    else:
        reject = 2;    
    return [reject,eta];


# the idea of this code is to approximate the pdf of a given normal distribution by generating samples and plotting a histogram. 
# I.e. we generate samples with a specified sampling strategy and count how many samples fall into a certain interval

method = "inverseSampling"; # pick sampling strategy from acceptReject, randn, inverseSampling, boxmuller

# define mean and variance
mu = 1.0;
sigma = 1.0;

# define grid for x (we will count the samples on this grid)
NGrid = 100;
width = 10;
grid = np.linspace(mu-width,mu+width,NGrid);
dx = grid[2]-grid[1];
gridMidpoints = np.linspace(mu-width+0.5*dx,mu+width-0.5*dx,NGrid-1);

# define hits. Hits saves how many samples fall into the interval (grid[l],grid[l+1]], i.e. we save a histogram
hits = np.zeros((NGrid-1,1));
success = 0;

# define number of samples to compute histogram
NSamples = 1000;

# main loop: Compute k-th sample with specified method and check in which part of the grid this sample will lie
for k in range(1,NSamples):
    reject = 1; # every sample is accepted if not otherwise defined. 1 is accepted, 2 is rejected
    
    # compute sample with mean 0, variance 1
    if method == "acceptReject":
        [reject,sample] = sampleNormal();
    elif method == "randn":
        sample = np.random.randn();
    elif method == "inverseSampling":
        U = np.random.rand();
        # Note: several special functions are implemented in scipy.special
        sample = scipy.special.erfinv(2*U-1)*np.sqrt(2);
    elif method == "boxmuller":
        U1 = np.random.rand(); U2 = np.random.rand();
        R = np.sqrt(-2*(np.log(U1)));
        theta = 2*np.pi*U2;
        sample = R*np.cos(theta);
    
    # scale sample to defined mean and variance
    sample = mu+sigma*sample;
    
    # save in hits: check in which part of the grid this sample will lie
    if reject == 1:
        success = success+1; # for acceptReject
        for l in range(1,NGrid-1):
            if sample > grid[l] and sample <= grid[l+1]:
                hits[l] = hits[l]+1;
                break;

if method == "acceptReject":
    print('efficiency is',success/NSamples)

# plotting
maxHits = max(hits); # rescale to exact pdf. One could also rescale such that histogram integrates to 1.

plt.figure(figsize=(20,10))
fig, ax = plt.plot(gridMidpoints,f(mu,mu,sigma)*hits/maxHits,gridMidpoints,f(gridMidpoints,mu,sigma))

plt.xlabel('x', fontsize=18);
plt.ylabel(r'$f_X(x)$', fontsize=18);
plt.legend([method,'exact'], fontsize=18);
plt.xticks(fontsize=18);
plt.yticks(fontsize=18);