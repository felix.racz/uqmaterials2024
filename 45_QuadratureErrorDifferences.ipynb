{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "baa9b854-0b9f-4a24-a6c4-85113726230b",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Uncertainty Quantification\n",
    "\n",
    "#### Prof. Dr. Martin Frank\n",
    "\n",
    "---\n",
    "\n",
    "## **Sparse Grid Error Estimates**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3322724b-1490-4965-a2c5-49825e11e90b",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "### **45 Quadrature Error of Differences**\n",
    "\n",
    "Let $\\alpha=1$ from now on. To study the error we write the true integral as the limit of an infinitely refined quadrature rule with index set $\\mathbb{N}^d$.\n",
    "$$\n",
    "\\left|Q^{(d)}g-Q_l^{(d)}g\\right|=\\left|\\sum_{k\\in\\mathbb{N}^d}\\Delta_{k}^{(d)}g-\\sum_{k\\in \\text{I}}\\Delta_{k}^{(d)}g \\right|\n",
    "$$\n",
    "where $k=(k_1,\\dots,k_d)$ is a multi-index and\n",
    "$$\n",
    "\\Delta_{k}^{(d)}g=\\left(\\Delta_{k_1}^{(1)} \\otimes \\cdots \\otimes \\Delta_{k_d}^{(1)}\\right) g\n",
    "$$\n",
    "Thus the error can be written as\n",
    "$$\n",
    "\\left|Q^{(d)}g-Q_l^{(d)}g\\right|=\\left|\\sum_{k\\in\\mathbb{N}^d\\setminus\\text{I}}\\Delta_{k}^{(d)}g \\right|\n",
    "$$\n",
    "\n",
    "**Lemma.**\n",
    "*Under the above smoothness assumption (and* $\\alpha=1$*)*\n",
    "\\begin{align*}\n",
    "\\left|\\Delta_{k}^{(d)}g\\right|\\leq C(g)2^{-|k|+d}.\n",
    "\\end{align*}\n",
    "\n",
    "**Proof:**\n",
    "The proof reuses a lot of ideas from the error bound of Clenshaw-Curtis quadratures. Let us start with a one-dimensional setting. Recall that we have\n",
    "\\begin{align*}\n",
    "\\Delta_{k}^{(1)}g = (Q_{k}^{(1)} - Q_{k-1}^{(1)})g.\n",
    "\\end{align*}\n",
    "Essentially, we need to now estimate the error of Clenshaw-Curtis quadratures at levels $k$ and $k-1$. We assume that the quadrature at level $k$ can integrate polynomials of degree $R_{k}$ exactly. Following the last lecture, we expand $g$ in Chebyshev polynomials\n",
    "\\begin{align*}\n",
    "g(x) = \\sum_{i=0}^{\\infty} g_{i} T_i(x).\n",
    "\\end{align*}\n",
    "\n",
    "With linearity of $\\Delta_{k}^{(1)}$, we have\n",
    "\\begin{align*}\n",
    "\\Delta_{k}^{(1)}g = \\Delta_{k}^{(1)} \\left(\\sum_{i=0}^{\\infty} g_{i} T_i\\right) = \\sum_{i=0}^{\\infty} g_{i}\\Delta_{k}^{(1)} T_i = \\sum_{i=0}^{\\infty} g_{i}\\left(Q_{k}^{(1)} T_i-Q_{k-1}^{(1)} T_i\\right).\n",
    "\\end{align*}\n",
    "Since the chosen CC quadrature at level $k-1$ is exact for polynomials up to order $N_{k-1}$, the first $N_{k-1}$ terms in the above sum are zero. Hence, we are left with\n",
    "\\begin{align*}\n",
    "\\Delta_{k}^{(1)}g = \\sum_{i=N_{k-1}+1}^{\\infty} g_{i} \\Delta_{k}^{(1)} T_i.\n",
    "\\end{align*}\n",
    "To check if this infinite sum has an upper bound, we note that\n",
    "\\begin{align*}\n",
    "\\left|\\Delta_{k}^{(1)}g\\right|\\leq& \\sum_{i=N_{k-1}+1}^{\\infty} \\left|g_{i}\\right| \\left|\\Delta_{k}^{(1)} T_i\\right|\n",
    "= \\sum_{i=N_{k-1}+1}^{\\infty} \\left|g_{i}\\right| \\left|(Q_{k}^{(1)} - Q_{k-1}^{(1)})T_i\\right|\\\\\n",
    "=& \\sum_{i=N_{k-1}+1}^{\\infty} \\left|g_{i}\\right| \\left|(Q_{k}^{(1)} - Q^{(1)}+ Q^{(1)} - Q_{k-1}^{(1)})T_i\\right|\\\\\n",
    "\\leq& \\sum_{i=N_{k-1}+1}^{\\infty} \\left|g_{i}\\right|\\Big( \\left|Q_{k}^{(1)}T_i- Q^{(1)}T_i\\right|+ \\left|Q_{k-1}^{(1)}T_i- Q^{(1)}T_i\\right|\\Big).\n",
    "\\end{align*}\n",
    "Recall that under our smoothness assumption we showed\n",
    "\\begin{align*}\n",
    "\\sum_{i=R_{k}+1}^{\\infty} \\left|g_{i}\\right|\\left|Q_{k}^{(1)}T_i- Q^{(1)}T_i\\right| = c(g)\\sum_{i=R_{k}+1}^{\\infty} i^{-\\alpha}\\left|Q_{k}^{(1)}T_i- Q^{(1)}T_i\\right| \\leq C(g)R_{k}^{-\\alpha}\n",
    "\\end{align*}\n",
    "to prove spectral convergence of CC quadratures in the last lecture. We could just use this as a justification why $\\left|\\Delta_{k}^{(1)}g\\right|\\leq C(g)N_{k-1}^{-\\alpha}$.\n",
    "However, let us make this statement more precise and underline the connection to last lecture. Recall that we showed\n",
    "\\begin{align*}\n",
    "\\left|Q_{k}^{(1)}T_i- Q^{(1)}T_i\\right| = |E_{k}(T_i)| = \\frac{2}{4r^2-1}-\\frac{2}{i^2 -1}.\n",
    "\\end{align*}\n",
    "Hence, following the last lecture with the assumption $g_i = O(i^{-\\alpha})$ we obtain\n",
    "\\begin{align*}\n",
    "\\left|\\Delta_{k}^{(1)}g\\right|\n",
    "\\leq& \\sum_{i=N_{k-1}+1}^{\\infty} \\left|g_{i}\\right|\\Big( |E_{k}(T_i)|+ |E_{k-1}(T_i)|\\Big)\\\\\n",
    "\\leq& C_1 \\frac{1}{(N_{k-1}-1)^{\\alpha}}\\sum_{|2r|\\leq N_{k-1}}\\frac{1}{4r^2-1}\\sum_{j=1}^{\\infty}\\frac{1}{(2j+2r/(N_{k-1}-1))^\\alpha} \\\\\n",
    "&+ C_2 N_{k-1}^{-\\alpha}\\sum_{i=N_{k-1}}^{\\infty}\\frac{1}{i^2} \\leq C(g)N_{k-1}^{-\\alpha}.\n",
    "\\end{align*}\n",
    "Let us now look at a two-dimensional function $g(x,y)$. Then we have with our smoothness assumption\n",
    "\\begin{align*}\n",
    "\\Delta_{k}^{(2)}g =& \\sum_{i=0}^{\\infty}\\sum_{j=0}^{\\infty} g_{ij} \\Delta_{k}^{(2)}\\left(T_i T_j\\right) = \\sum_{i=0}^{\\infty}\\sum_{j=0}^{\\infty} g_{ij} \\Delta_{k_1}^{(1)}T_i\\Delta_{k_2}^{(1)}T_j \\\\\n",
    "=& \\sum_{i=N_{k_1-1}+1}^{\\infty}\\sum_{j=N_{k_2-1}+1}^{\\infty}g_{ij} \\Delta_{k_1}^{(1)}T_i \\cdot\\Delta_{k_2}^{(1)}T_j\\\\\n",
    "=& C(g)\\sum_{i=N_{k_1-1}+1}^{\\infty}i^{-\\alpha}\\Delta_{k_1}^{(1)}T_i\\cdot\\sum_{j=N_{k_2-1}+1}^{\\infty}j^{-\\alpha}\\Delta_{k_2}^{(1)}T_j\n",
    "\\end{align*}\n",
    "Hence, according to our bound for the one-dimensional problem we get\n",
    "\\begin{align*}\n",
    "\\left\\vert \\Delta_{k}^{(2)}g \\right\\vert \\leq C(g)\\cdot N_{k_1-1}^{-\\alpha} N_{k_2-1}^{-\\alpha}.\n",
    "\\end{align*}\n",
    "In $d$ dimensions we then have\n",
    "\\begin{align*}\n",
    "\\left\\vert \\Delta_{k}^{(d)}g \\right\\vert \\leq C(g) \\prod_{i = 1}^d N_{k_i-1}^{-\\alpha}.\n",
    "\\end{align*}\n",
    "Using $N_{k_i-1} = 2^{k_i-1}$ and $\\alpha = 1$, we get\n",
    "\\begin{align*}\n",
    "\\left\\vert \\Delta_{k}^{(d)}g \\right\\vert \\leq C(g) 2^{-|k|+d}.\n",
    "\\end{align*}\n",
    "$$\\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\square$$"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
