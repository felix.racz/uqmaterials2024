{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "baa9b854-0b9f-4a24-a6c4-85113726230b",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Uncertainty Quantification\n",
    "\n",
    "#### Prof. Dr. Martin Frank\n",
    "\n",
    "---\n",
    "\n",
    "## **Multi-level Monte Carlo and Monte Carlo again**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3322724b-1490-4965-a2c5-49825e11e90b",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "### **57 Discussion**\n",
    "UQ (as propagation of uncertainties through models) effectively boils down to high-dimensional integration. We approximate a true integral $Qg = \\int_{[0,1]^d}g$ by a quadrature rule $Q^{(N)}g$, which uses $N$ points in total. We (in this course) say that the method suffers from the Curse of Dimensionality when the number of points $N$ necessary to achieve a certain accuracy increases exponentially with $d$.\n",
    "\n",
    "We have considered different types of methods \n",
    "\n",
    "- They can be classified as intrusive (we have to change the model or code)\n",
    "- Non-intrusive (we only require that we evaluate the model many times, run the model as black box)\n",
    "- All methods that were presented are non-intrusive except for Stochastic Galerkin. \n",
    "\n",
    "\n",
    "**Example.** (Monte Carlo)\n",
    "\n",
    "- MC error \n",
    "$$\n",
    "E[(Qg-Q^{(N)}g)^2]^{1/2} = \\sqrt{\\frac{\\text{Var}(g)}{N}}\n",
    "$$\n",
    "- Variance\n",
    "$$\n",
    "\\text{Var}(g) = \\left(\\int_{[0,1]^d} (g-Qg)^2\\right)^{1/2}\n",
    "$$\n",
    "- MLMC is a (smart) variance reduction\n",
    "\n",
    "**Example.** (Tensorized Grids)\n",
    "\n",
    "- The error for composite rule of order $\\alpha$ is\n",
    "$$\n",
    "|Qg-Q^{(N)}g|   \\leq \\max_z \\left(\\sum_{i=1}^d (\\frac{\\partial^\\alpha g}{\\partial z_i^\\alpha})^2\\right)^{1/2} N^{-\\alpha/d}\n",
    "$$\n",
    "- Curse of dimensionality\n",
    "\n",
    "**Example.** (Stochastic Galerkin)\n",
    "- Not a quadrature. Obtain an equation for QoI directly by gPC expansion \n",
    "\n",
    "$$\n",
    "u^{(N)}(z) = \\sum_{i} u_i \\psi_i(z),\\quad i = (i_1,\\dots,i_d)\n",
    "$$\n",
    "and Galerkin projection\n",
    "\n",
    "$$\n",
    "E[\\mathcal{E}(u^{(N)}(Z),Z)\\psi_j(Z)] = 0\n",
    "$$\n",
    "- The error typically depends on best approximation error\n",
    "\n",
    "$$\n",
    "\\inf_{u^*\\in \\text{span}(\\psi_i} (E[|u-u^*|^2])^{1/2}\n",
    "$$\n",
    "- The Stochastic Galerkin method typically uses a spectral expansion to overcome the curse of dimensionality. It has the same advantages as \"standard\" Galerkin: it inherits structure of the original problem, error estimates and possibly adaptivity.\n",
    "- In a non-intrusive method, we solve many forward problems that are quite similar, but we make no use of it. Stochastic Galerkin, on the other hand, couples the differential parameter values so that one gets a coupled system. The hope is that a coupling exploits that the problems with slightly different parameters are similar.\n",
    "- Stochastic Galerkin uses the minimal number of degrees of freedom to describe polynomials of a certain total degree.\n",
    "\n",
    "    - Polynomial\n",
    "        $$\n",
    "            u^{(N)}(z) = \\sum_{i} u_i \\psi_i(z)\n",
    "        $$\n",
    "    - Total degree \n",
    "        $$\n",
    "            |i| = i_1+\\dots +i_d \\leq n, \\quad \\binom{n+d}{d}\\text{ terms}\n",
    "        $$\n",
    "    - Max degree\n",
    "        $$\n",
    "            \\max i = \\max{i_1,\\dots,i_d} \\leq n , \\quad n^d\\text{ terms}\n",
    "        $$\n",
    "\n",
    "All other methods use (significantly) more points.\n",
    "\n",
    "**Example.** (Sparse Grids)\n",
    "\n",
    "- Smoothness $g\\in H^s$ means a certain decay rate of Fourier coefficients\n",
    "\n",
    "$$\n",
    "g(z) = \\sum_{m=0}^\\infty a_m T_m(z),\\quad a_m = \\mathcal{O}(m^{-s})\n",
    "$$\n",
    "- Proof using that orthogonal polynomials are eigenfunctions of a symmetric, 2nd order differential operator\n",
    "- We make a crucial assumption for the sparse grid error estimate\n",
    "\n",
    "\\begin{gather*}\n",
    "g(z_1,\\dots,z_d) = \\sum_{m_1=0}^\\infty\\cdots \\sum_{m_d=0}^\\infty a_{m_1 \\cdots m_d} T_{m_1}(z_1)\\cdots T_{m_d}(z_d)\\\\\n",
    "a_{m_1 \\cdots m_d} = \\mathcal{O}((m_1\\cdots m_d)^{-s})\n",
    "\\end{gather*}\n",
    "- This means that $\\frac{\\partial^{s\\cdot d}g}{\\partial z_1^s\\dots \\partial z_d^s}$ exists; this means that the smoothness of $g$ increases with the dimension!\n",
    "- Error estimate\n",
    "$$\n",
    "\\varepsilon = \\mathcal{O}((\\log N)^dN^{-s})\n",
    "$$\n",
    "\n",
    "**Example.** (Quasi-Monte Carlo)\n",
    "\n",
    "- Hardy-Krause variation\n",
    "\n",
    "$$\n",
    "g:[0,1]^d\\rightarrow\\mathbb{R}: V^{HK}(g)=\\int_{[0,1]^d}\\left|\\frac{\\partial^d g}{\\partial z_1\\partial z_2\\dots\\partial z_d}\\right|dz+\\sum_{i=1}^{d}V^{HK}\\left( g_{1}^{(i)}\\right)\n",
    "$$\n",
    "As with sparse grids, this means that the smoothness of $g$ increases with the dimension!\n",
    "- Discrepancy\n",
    "\n",
    "\\begin{gather*}\n",
    "D_{N}^0:=\\sup\\limits_{j\\in E^0}|R_n(\\mathcal{J})|, \\quad R_N(\\mathcal{J})=\\frac{1}{N}\\left|\\left( z^{(i)}\\right) \\cap\\mathcal{J}\\right|-\\text{vol}\\ (\\mathcal{J})\\\\ E^0=\\{\\mathcal{J}(x)=[0,x_1]\\times\\dots\\times[0,x_d]:x\\in[0,1]^d\\}\n",
    "\\end{gather*}\n",
    "- Koksma-Hlawka: Error for any set of points\n",
    "\n",
    "$$\n",
    "|Qg-Q^{(N)}g|   \\leq V^{HK}(g)\\cdot D_N^0\n",
    "$$\n",
    "- Minimal discrepancy sequences have\n",
    "$$\n",
    "D_N^0 = \\mathcal{O}((\\log N)^d N^{-1})\n",
    "$$\n",
    "- Mitigates curse of dimensionality\n",
    "- The error estimate of QMC contains two terms, the Hardy-Krause variation (Chapter 6), and the discrepancy, which are not isotropic. By isotropic we mean that a quantity is invariant under rotations of the coordinate system. The set of total degree polynomials is isotropic, the set of max degree polynomials is not isotropic. The sparse grid error estimate required the existence of certain high-order mixed derivatives, which again means anisotropy.\n",
    "\n",
    "A very simple example:\n",
    "$$f(x)=\\sin{\\pi k x_1}$$ \n",
    "on $[0,1]^d$ with rotation\n",
    "\\begin{align*}\n",
    "  \\begin{bmatrix}\n",
    "    x_1 \\\\\n",
    "    0\\\\\n",
    "    \\vdots \\\\\n",
    "    0\n",
    "    \\end{bmatrix}\n",
    "    \\mapsto \\frac{1}{\\sqrt{d}} \\begin{bmatrix}\n",
    "    1 \\\\\n",
    "    1\\\\\n",
    "    \\vdots \\\\\n",
    "    1\n",
    "    \\end{bmatrix} \n",
    "  \\end{align*}\n",
    "the function becomes\n",
    "$$g(x)=\\sin\\left( k\\pi d^{-\\frac{1}{2}}(x_1+\\dots+x_d)\\right)$$\n",
    "For these functions \n",
    "\\begin{align*}\n",
    "V^{HK}(f)&\\sim 2k\\\\\n",
    "V^{HK}(g)&\\sim \\left( k\\pi d^{-\\frac{1}{2}}\\right)^d\n",
    "\\end{align*}\n",
    "One can take away two points for this example:\n",
    "\n",
    " - QMC should not be much worse for $g$ than for $f$. This means that the estimate might not be sharp. This means that the Koksma-Hlawka inequality not necessarily explains why QMC works (if it works). On the other hand if a function is anisotropic in the sense that its $V^{HK}$ is small, we do know that QMC works.\n",
    " - Integrating this function with a sparse grid also means that we use anisotropy.\n",
    "\n",
    "\n",
    "\n",
    "> *The only way to fight the curse of dimensionality is to identify and exploit special structure in the model.*"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
